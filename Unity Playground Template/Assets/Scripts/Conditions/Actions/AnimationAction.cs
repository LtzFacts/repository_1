﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

// TODO: Convert to proper Action base class with Execute function

[AddComponentMenu("Playground/Actions/Animation")]
public class AnimationAction : MonoBehaviour
{

    public Animator objectToAnimate;
    public string parameterName;


    public void Start()
    {
        // If no specific object was requested to be animated, assume it either on the current object
        if (objectToAnimate == null)
        {
            objectToAnimate = GetComponent<Animator>();
        }

        if (objectToAnimate == null)
        {
            Debug.LogError("Failed to animate - no animator provided and no animator found on object " + name);
        }
    }


    // Sets a trigger for this animator
    public void SetTrigger(string _parameterName)
    {
        // If we have an object to animate...
        if (objectToAnimate != null)
        {
            // Change the parameter on this animator based on our settings
            objectToAnimate.SetTrigger(_parameterName);
        }
    }


    // Sets a bool for this animator
    public void SetBool(bool parameterValue)
    {
        // If we have an object to animate...
        if (objectToAnimate != null)
        {
            // Change the parameter on this animator based on our settings
            objectToAnimate.SetBool(parameterName, parameterValue);
        }
    }



    // Sets an int for this animator
    public void SetInt(int parameterValue)
    {
        // If we have an object to animate...
        if (objectToAnimate != null)
        {
            // Change the parameter on this animator based on our settings
            objectToAnimate.SetInteger(parameterName, parameterValue);
        }
    }



    // Sets a float for this animator
    public void SetFloat(float parameterValue)
    {
        // If we have an object to animate...
        if (objectToAnimate != null)
        {
            // Change the parameter on this animator based on our settings
            objectToAnimate.SetFloat(parameterName, parameterValue);
        }
    }

}
