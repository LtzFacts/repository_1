using UnityEngine;
using System.Collections;

[AddComponentMenu("Playground/Movement/Rotate")]
[RequireComponent(typeof(Rigidbody2D))]
public class Rotate : Physics2DObject
{

    public enum RotationControls
    {
        ArrowKeys,
        WASD,
        Mouse
    }

    [Header("Input keys")]
	public RotationControls typeOfControl = RotationControls.ArrowKeys;

	[Header("Rotation")]
	public float speed = 5f;

    private float spin;


    // Update gets called every frame
    void Update ()
	{	
		// Register the spin from the player input
		// Moving with the arrow keys
		if(typeOfControl == RotationControls.ArrowKeys)
		{
			spin = Input.GetAxis("Horizontal");
		}
		else if (typeOfControl == RotationControls.WASD)
		{
			spin = Input.GetAxis("Horizontal2");
		}
        else
        {
            float currentAngle = transform.rotation.eulerAngles.z;


            // Determine direction to target
            Vector2 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 currentPos = transform.position;
            Vector2 directionToTarget = (mouseWorldPos - currentPos).normalized;
            float targetAngle = Mathf.Rad2Deg * Mathf.Atan2(directionToTarget.y, directionToTarget.x);
            float angleDifference = currentAngle - targetAngle;

            // Wrap around, get angle in terms of left or right (between -Pi and Pi)
            while (angleDifference > 180f)
                angleDifference -= 360f;
            while (angleDifference < -180f)
                angleDifference += 360f;

            // Rotate towards player
            if (Mathf.Abs(angleDifference) < 1f) // Would pass target angle, so just set to it
            {
                spin = 0;
                rigidbody2D.angularVelocity = 0;
                transform.rotation = Quaternion.Euler(0, 0, targetAngle);
            }
            else if (angleDifference < 0f) // Should turn left
                spin = -1;
            else if (angleDifference > 0f) // Should turn right
                spin = 1;
        }
	}
	

	// FixedUpdate is called every frame when the physics are calculated
	void FixedUpdate ()
	{
		// Apply the torque to the Rigidbody2D
		rigidbody2D.AddTorque(-spin * speed);
	}
}
